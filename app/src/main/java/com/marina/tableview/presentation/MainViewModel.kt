package com.marina.tableview.presentation

import android.graphics.Color
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.marina.tableview.R
import com.marina.tableview.domain.TableViewRepository
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val repository: TableViewRepository
) : ViewModel() {

    private var _imageName = MutableLiveData<Int>()
    val imageName: LiveData<Int> get() = _imageName

    private var _buttonColor = MutableLiveData<Int>()
    val buttonColor: LiveData<Int> get() = _buttonColor

    init {
        _imageName.value = R.drawable.capi7
        _buttonColor.value = Color.rgb(209, 159, 0)
    }

    fun getNewImage() {
        val name = repository.getRandomImageID()
        _imageName.postValue(name)
    }

    fun getNewColor() {
        val color = repository.getRandomColor()
        _buttonColor.postValue(color)
    }
}