package com.marina.tableview.presentation

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.marina.tableview.app.App
import com.marina.tableview.databinding.ActivityMainBinding
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    @Inject
     lateinit var viewModel: MainViewModel

    private val component by lazy {
        (this.application as App).component
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        observeViewModel()
        setOnClickListener()
    }

    private fun observeViewModel() {
        viewModel.imageName.observe(this) { resource ->
            Log.e(this.javaClass.simpleName, resource.toString())
            binding.ivCapi.setImageResource(resource)
        }

        viewModel.buttonColor.observe(this) { color ->
            Log.e(this.javaClass.simpleName, color.toString())
            binding.btnNextCapi.setBackgroundColor(color)
        }
    }

    private fun setOnClickListener() {
        binding.btnNextCapi.setOnClickListener {
            viewModel.getNewColor()
            viewModel.getNewImage()
        }
    }
}