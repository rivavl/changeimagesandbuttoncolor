package com.marina.tableview.data

import android.graphics.Color
import com.marina.tableview.R
import com.marina.tableview.domain.TableViewRepository
import javax.inject.Inject
import kotlin.random.Random

class TableViewRepositoryImpl @Inject constructor() : TableViewRepository {

    override fun getRandomImageID(): Int {
        return IMAGES[Random.nextInt(0, IMAGES.lastIndex)]
    }

    override fun getRandomColor(): Int {
        return Color.rgb(
            Random.nextInt(RGB_MOD),
            Random.nextInt(RGB_MOD),
            Random.nextInt(RGB_MOD)
        )
    }

    companion object {
        private val IMAGES =
            mutableListOf(
                R.drawable.capi10,
                R.drawable.capi11,
                R.drawable.capi12,
                R.drawable.capi9,
                R.drawable.capi8,
                R.drawable.capi7,
                R.drawable.capi6,
                R.drawable.capi5,
                R.drawable.capi4
            )

        private val RGB_MOD = 256
    }
}