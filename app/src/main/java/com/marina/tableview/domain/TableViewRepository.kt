package com.marina.tableview.domain

interface TableViewRepository {

    fun getRandomImageID(): Int

    fun getRandomColor(): Int
}