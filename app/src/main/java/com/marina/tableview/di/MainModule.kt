package com.marina.tableview.di

import com.marina.tableview.data.TableViewRepositoryImpl
import com.marina.tableview.di.annotation.ApplicationScope
import com.marina.tableview.domain.TableViewRepository
import com.marina.tableview.presentation.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
interface MainModule {
    @ApplicationScope
    @Binds
    fun bindRepository(impl: TableViewRepositoryImpl): TableViewRepository

    companion object {
        @ApplicationScope
        @Provides
        fun provideMainViewModel(repository: TableViewRepository): MainViewModel {
            return MainViewModel(repository)
        }
    }
}