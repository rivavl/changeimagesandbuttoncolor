package com.marina.tableview.di

import com.marina.tableview.di.annotation.ApplicationScope
import com.marina.tableview.presentation.MainActivity
import dagger.Component

@ApplicationScope
@Component(modules = [MainModule::class])
interface AppComponent {

    fun inject(activity: MainActivity)
}