package com.marina.tableview.app

import android.app.Application
import com.marina.tableview.di.DaggerAppComponent

class App : Application() {

    val component by lazy {
        DaggerAppComponent.builder().build()
    }
}